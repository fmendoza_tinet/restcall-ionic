var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json([
    {
      id: 1,
      message: "Primer mensaje"
    },
    {
      id: 2,
      message: "Segundo mensaje"
    },
    {
      id: 3,
      message: "Ultimo mensaje"
    }
  ])
  // res.render('index', { title: 'Express' });
});

router.get('/otro', function(req, res, next) {
  res.json([
    {
      id: 1,
      message: "Hello ionic"
    },
    {
      id: 2,
      message: "Segundo mensaje"
    },
    {
      id: 3,
      message: "Final message"
    }
  ])
  // res.render('index', { title: 'Express' });
});

module.exports = router;
