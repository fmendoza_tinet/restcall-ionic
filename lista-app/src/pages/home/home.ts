import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IonicPage } from 'ionic-angular';
import { ListaServiceProvider } from '../../providers/lista-service/lista-service';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  listado = [];

  constructor(private lista: ListaServiceProvider) {
    this.getList();
  }

  getList() {
    this.lista.getList().subscribe(data => this.listado = data);
  }
}
